﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaGenericEndava
{
    class Program
    {
        static T[] ArrayReverse<T>(T[] vector)
        {
            T[] vectRev = new T[vector.Length];
            int i = 0;
            foreach (T el in vector.Reverse())
            {
                vectRev[i] = el;
                i++;
            }
            return vectRev;
        }
        static void Main(string[] args)
        {
           
            int[] vector = new int[25];
            for (int i = 0; i < 25; i++)
                vector[i] = i;
            vector = ArrayReverse(vector);
            for (int i = 0; i < 25; i++)
                Console.Write(vector[i]+ " ");
            Console.WriteLine();
            Console.WriteLine();



            Dictionary<int, string> dict = new Dictionary<int, string>();
            dict.Add(1, "Ionel");
            dict.Add(2, "Vasile");
            //dict.Add(2, "Ionel"); - nu accepta duplicate, va returna o exceptie
            HashSet<int> hash = new HashSet<int>();
            hash.Add(1);
            hash.Add(2);
            hash.Add(1);
            dict.ToList().ForEach(x => Console.WriteLine(x.Key + " " + x.Value));
            var jn = string.Join(", ", hash);
            Console.WriteLine(jn);



            GenericCls<object,Interfata> cls = new TemaGenericEndava.GenericCls<object, Interfata>();
            cls.Afis();
            Order p1 = new Order(20, "XL");
            Order p2 = new Order(70, "S");
            Order p3 = new Order(40, "L");
            Order p4 = new Order(50, "M");
            Order p5 = new Order(100, "3XL");
            Customer c1 = new Customer("Ion", 29);
            c1.AdaugaCom(p1);
            c1.AdaugaCom(p4);
            c1.AdaugaCom(p2);
            Customer c2 = new Customer("Vasile", 18);
            c2.AdaugaCom(p3);
            c2.AdaugaCom(p5);
            c2.AdaugaCom(p3);
            Customer c3 = new Customer("Tom", 55);
            c3.AdaugaCom(p1);
            c3.AdaugaCom(p5);
            Dictionary<string, List<Order> >dic = new Dictionary<string, List<Order>>();
            dic.Add(c1.Name, c1.comenzi);
            dic.Add(c2.Name, c2.comenzi);
            dic.Add(c3.Name, c3.comenzi);
            Console.WriteLine();
            foreach (KeyValuePair<string, List<Order>> x in dic)
            {
                Console.Write(x.Key + " Comanda: ");
                foreach (Order y in x.Value)
                {
                    Console.Write(y.ToString()+" ");
                }

                Console.WriteLine();
            }
            Console.WriteLine();


            List<Customer> lstCst = new List<Customer>();
            Customer c4 = new Customer("Ionut", 29);
            c1.AdaugaCom(p2);
            c1.AdaugaCom(p3);
            c1.AdaugaCom(p2);
            Customer c5 = new Customer("Vasilescu", 18);
            c2.AdaugaCom(p1);
            c2.AdaugaCom(p5);
            c2.AdaugaCom(p4);
            Customer c6 = new Customer("Tatiana", 55);
            c3.AdaugaCom(p2);
            c3.AdaugaCom(p3);
            c1.Create(lstCst);
            c2.Create(lstCst);
            c3.Create(lstCst);
            foreach (Customer x in lstCst)
            {
                Console.WriteLine(x.Read());              
            }
            c2.Delete(lstCst);
            Console.WriteLine();
            foreach (Customer x in lstCst)
            {
                Console.WriteLine(x.Read());
            }

            Console.ReadKey();
        }
    }

    class Customer : IGeneric<Customer>
    {
        public void Create(List <Customer> x)
        {
            if (this != null)
                x.Add(this);
        }

        public void Delete(List<Customer> y)
        {
            if (this != null)
                y.Remove(this);
        }

        public string Read()
        {
            return ToString();
        }

        public void Update(object T)
        {
            
        }
    
        public string Name { get; set; }
        public int Age { get; set; }
        public List<Order> comenzi = new List<Order>();
        public Customer(string _nume, int _varsta)
        {
            Name = _nume;
            Age = _varsta;
        }

        public void AdaugaCom(Order x)
        {
            comenzi.Add(x);
        }

        public override string ToString()
        {
            return Name + " " + Age+ " ani";
        }

    }

    public class Order : IGeneric<Order>
    {
        int Value { get; set; }
        string Size { get; set; }
        
        public Order(int _valoare, string _marime)
        {
            Value = _valoare;
            Size = _marime;
        }
        public override string ToString()
        {
            return Value+"-"+Size;
        }

        public string Read()
        {
            return ToString();
        }

        public void Create(List<Order> x)
        {
            if (this != null)
                x.Add(this);
        }

        public void Update(object T)
        {
           
        }

        public void Delete(List<Order> y)
        {
            if (this != null)
                y.Remove(this);
        }
    }

    class GenericCls<T1, T2>
        where T1 : class, new()
        where T2 : Interfata
    {
        public static T1 var1 = new T1();
        public static T2 var2 ;
        public void Afis()
        {
            Console.WriteLine(var1.GetType()
                //+" "+var2.GetType()
                );
        }
        public GenericCls()
        {
            

        }
    }

    interface Interfata
    {
       
    }

    interface IGeneric <T>
    {
        string Read();
        void Create(List <T> x);
        void Update(object T);
        void Delete(List <T> y);
    }

    
}
